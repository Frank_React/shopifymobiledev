export const CONST_INIT_NAVIGATIONS = "CONST_INIT_NAVIGATIONS";

export function initNav(){
	return {
		type: CONST_INIT_NAVIGATIONS
	}
};