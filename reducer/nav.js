
/* This reducer handles any navigation values

Navigation:
	{
		collectionId: null by default otherwise it's an int of the collection selected,
		productId: null by default otherwise it's an int of the product selected
	}

*/

import {CONST_INIT_NAVIGATIONS} from "../action/nav";

export function nav(state = {}, action) {
	switch (action.type) {
		case CONST_INIT_NAVIGATIONS:
			return {
				collectionId: null, // this is the selected collection
			};
		default:
			return state;
	}

}