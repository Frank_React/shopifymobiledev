import React from 'react';
import {CardItem, Text, View} from "native-base";
import {material} from "react-native-typography";
import {Image, ScrollView, StyleSheet} from 'react-native';
import {Button, Icon, View, Text, CardItem} from "native-base";
import {material} from "react-native-typography";

export default class SelfPromotion extends React.Components {
	render(){
		return (
				<View styles={{flex: 1}}>
					<CardItem bordered>
						<Text style={material.body1}>This React Native App was created by Frank Haolun Li on 01.19.2019 as part of a
							job application for Shopify.</Text>
					</CardItem>
					<CardItem bordered>
						<Text style={material.body1}>Email: 123frankemail@gmail.com</Text>
					</CardItem>
					<CardItem bordered>
						<Text style={material.body1}>LinkedIn: @123frank</Text>
					</CardItem>
					<CardItem bordered>
						<Text style={material.body1}>If you would like help with your own React Native App or would like to hire me
							feel free to email me!</Text>
					</CardItem>
					<CardItem bordered>
						<Text style={material.body1}>To see the source code visit: https://gitlab.com/Frank_React/shopifymobiledev</Text>
					</CardItem>
				</View>
		)
	}
}