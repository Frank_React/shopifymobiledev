import React, {Component} from 'react';
import {connect, FlatList} from "react-redux";
import {Text} from "native-base";
import {ScrollView, } from "react-native";
import {CONST_RECENT_SALES_URLS} from "./RecentSalesStatic";
import CircularImage from "./RecentSalesScrollView";
import CategoryCard from "./CategoryCard"
import { Card } from 'native-base';
import Loading from "../../Loading"

/* This component is going to use a FlatList since it's actually getting
 * stuff from the server  */
class CategoryListView extends Component {
	render() {
		return (
				<React.Fragment>
					{
						this.props.loading
								? <Loading />
								: this.renderEachCollection()
					}
				</React.Fragment>
		)
				;
	}

	renderEachCollection() {
		/* This method returns all the collections in their own card */
		return (
				<ScrollView>

						{
							this.props.collections.map((item, i) => {
								return (
										<CategoryCard
											key = {`CategoryCardComponent-i:${i}`}
											collection={item}/>
								)
							})
						}
				</ScrollView>
		)
	}
}

function mapStateToProps({collections}) {
	return {
		collections: collections.custom_collections,
		loading: collections === undefined || collections.custom_collections === undefined
	};
}

export default connect(mapStateToProps)(CategoryListView);
