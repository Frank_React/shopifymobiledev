export const CONST_RECENT_SALES_URLS = [
	'https://i.imgur.com/iC6AGWL.jpg',
	'https://i.imgur.com/ROPvau3.jpg',
	'https://i.imgur.com/ncCS4dq.jpg',
	'https://i.imgur.com/Mc5WnFM.jpg',
	'https://i.imgur.com/PthHoIr.jpg',
	'https://i.imgur.com/PFbRJIA.jpg',
	'https://i.imgur.com/0TEfr3s.jpg',
	'https://i.imgur.com/VjfW3fK.jpg'
];

const ACTUAL_ROBOTS = "https://cdn.shopify.com/s/files/1/1000/7970/products/Aerodynamic_20Concrete_20Clock.png?v=1443055734,2759137027\n" +
		"https://cdn.shopify.com/s/files/1/1000/7970/products/Aerodynamic_20Granite_20Plate.png?v=1443055768,2759143811\n" +
		"https://cdn.shopify.com/s/files/1/1000/7970/products/Aerodynamic_20Linen_20Computer.png?v=1443055794,2759149635\n" +
		"https://cdn.shopify.com/s/files/1/1000/7970/products/Aerodynamic_20Rubber_20Shoes.png?v=1443055892,2759170115\n" +
		"https://cdn.shopify.com/s/files/1/1000/7970/products/Aerodynamic_20Wooden_20Table.png?v=1443055911,2759180227\n" +
		"https://cdn.shopify.com/s/files/1/1000/7970/products/Awesome_20Bronze_20Bag.png?v=1443055746,2759139395\n" +
		"https://cdn.shopify.com/s/files/1/1000/7970/products/Awesome_20Bronze_20Computer.png?v=1443055880,2759167747\n" +
		"https://cdn.shopify.com/s/files/1/1000/7970/products/Awesome_20Concrete_20Watch.png?v=1443055782,2759147203\n" +
		"https://cdn.shopify.com/s/files/1/1000/7970/products/Awesome_20Copper_20Chair.png?v=1443055799,2759150915\n" +
		"https://cdn.shopify.com/s/files/1/1000/7970/products/Awesome_20Cotton_20Computer.png?v=1443055821,2759155139\n" +
		"https://cdn.shopify.com/s/files/1/1000/7970/products/Awesome_20Cotton_20Table.png?v=1443055886,2759168835\n";


export function getRobots(){
	const splitByImage = ACTUAL_ROBOTS.split("\n");
	return splitByImage;
}
