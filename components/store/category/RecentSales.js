import React from 'react';
import {connect} from "react-redux";
import {Body, Card, CardItem, Text} from "native-base";
import RecentSalesScrollView from "./RecentSalesScrollView";
import {material} from 'react-native-typography'
import {withNavigation} from "react-navigation";

const style = {
	font: {
		fontSize: 10,
		paddingTop: 10,
	}
};

class RecentSales extends React.Component {

	render() {
		return (
				<Body>
					<RecentSalesScrollView/>
					<Text style={material.caption}>
						Recent Sales
					</Text>
				</Body>
		);
	}



}


export default connect()(withNavigation(RecentSales));