import React, { Component } from 'react';
import { View, StyleSheet, Image } from 'react-native';


/*
This is a custom component that loads the circular images
expects the prop: the name of the image file located in img*/
export default class CircularImage extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image
          style={styles.image}
          resizeMode={"cover"}
          source={{ uri: this.props.url}}
        />
      </View>
    );
  }
}
const dimensions = 70;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 5,
	  paddingBottom: 5,
	  paddingRight: 8,
  },
  image: {
    height: dimensions,
    width: dimensions,
    borderRadius: 40,
  }
});
