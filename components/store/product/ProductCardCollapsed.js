import React from 'react';
import {Text, CardItem, Container} from "native-base";
import {View, TouchableWithoutFeedback,} from 'react-native';
import {connect} from 'react-redux';
import {material} from 'react-native-typography';
import CircularImage from '../CircularImage';
import ProgressBar from "./ProgressBar";
import Icon from 'react-native-vector-icons/FontAwesome';

class ProductCardCollapsed extends React.Component {

	render() {
		const {product} = this.props;
		const imageUrl = product.image.src;
		return (
				<TouchableWithoutFeedback onPress={() => this.props.changeCardState()}>
					<View>
						<View style={Styles.imageRight}>
							{/* The image to the left */}
							<View style={{width: 70}}>
								<CircularImage url={imageUrl}/>
							</View>
							<View style={{width: 210}}>
								{collectionAndBody(
										product.title,
										product.body_html,
										this.props.totalInventory,
										this.props.amountSold)
								}
							</View>
							<View style={Styles.centerVertically}>
								<Icon size={20} name='expand' styles={{color: '#CCD0DB'}}/>
							</View>
						</View>
					</View>
				</TouchableWithoutFeedback>

		)
				;
	}
}

function collectionAndBody(title, body, totalInventory, amountSold) {
	/* title: String
	*  body: String
	*  Return's a View with the proper styling*/

	let bodyContent = body;
	if (bodyContent === "") {
		bodyContent = "Hmm, no description here."
	}

	const decimalAmount = amountSold / totalInventory;
	const percent = Math.round(decimalAmount * 100);
	return (
			<View>
				<View style={Styles.titleAndDescription}>
					<Text style={material.body1}>{title}</Text>
					<Text style={material.caption}>{bodyContent}</Text>
				</View>
				<View style={{paddingTop: 9}}>
					<ProgressBar progress={decimalAmount} width={100}/>
					<Text style={material.caption}>{percent}% Sold ({amountSold}/{totalInventory})</Text>
				</View>
			</View>
	)

}


const Styles = {
	backButton: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
		padding: 10
	},
	centerVertically: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center'
	},
	imageRight: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-around'
	},
	titleAndDescription: {
		flex: 1,
		flexDirection: 'column',
		paddingTop: 20,
	}
};

export default ProductCardCollapsed;