import React from 'react';
import {Text, CardItem, Container, Card} from "native-base";
import {View, TouchableWithoutFeedback,} from 'react-native';
import {connect} from 'react-redux';
import {material} from 'react-native-typography';
import CircularImage from '../CircularImage';
import ProgressBar from "./ProgressBar";
import Icon from 'react-native-vector-icons/FontAwesome';
import {random} from "./ProductCard"

/*
 In the uncallaposed view we will want to show more details about the product
 including it's subtypes
{
  "id": 2759137027,
  "title": "Aerodynamic Concrete Clock",
  "body_html": "Transition rich vortals",
  "vendor": "Jenkins, Orn and Blick",
  "product_type": "Clock",
  "tags": "Aerodynamic, Clock, Concrete",
  "variants": [
    {
      "id": 8041741187,
      "product_id": 2759137027,
      "title": "Mint green",
      "price": "4.32",
      "position": 1,
      "inventory_policy": "deny",
      "fulfillment_service": "manual",
      "option1": "Mint green",
      "taxable": true,
      "grams": 4917,
      "image_id": null,
      "weight": 4.917,
      "weight_unit": "kg",
      "inventory_item_id": 6015629699,
      "inventory_quantity": 134,
      "old_inventory_quantity": 134,
      "requires_shipping": true,
    },

  ],
}
*/
class ProductCardUncollapsed extends React.Component {

	render() {
		const {product} = this.props;
		const imageUrl = product.image.src;
		return (
				<TouchableWithoutFeedback onPress={() => this.props.changeCardState()}>
					<View>
						{/* The first row*/}
						<View style={Styles.imageRight}>
							{/* The image to the left */}
							<View style={{width: 70}}>
								<CircularImage url={imageUrl}/>
							</View>
							{/* this it the text in the middle*/}
							<View style={{width: 210}}>
								{collectionAndBody(
										product.title,
										product.body_html,
										this.props.totalInventory,
										this.props.amountSold)
								}
							</View>
							{/* this is the toggle button*/}

							<View style={{width: 50, paddingTop: 30,}}>
								<Icon size={20} name='compress' styles={{color: '#CCD0DB'}}/>
							</View>

						</View>
						{/* The second row: additional details*/}
						<View style={Styles.imageRight}>
							<View style={Styles.imageRight}>
								<View style={Styles.titleAndDescription}>
									<Text style={material.caption}>{`Vendor: ${product.vendor}`}</Text>
									<Text style={material.caption}>{`Product Type: ${product.product_type}`}</Text>
									<Text style={material.caption}>{`Tags: ${product.tags}`}</Text>
									{this.renderVariations()}
								</View>
							</View>
						</View>
					</View>
				</TouchableWithoutFeedback>

		);
	}

	renderVariations() {
		/* get variations info*/
		return (
				<React.Fragment>
					{
						this.props.product.variants.map((obj, index) => {
							return variantionViews(obj, index);
						})
					}
				</React.Fragment>
		)
	}
}

function variantionViews(variationObj, index) {
	/* {
		"id": 8041741187,
		"product_id": 2759137027,
		"title": "Mint green",
		"price": "4.32",
		"position": 1,
		"inventory_policy": "deny",
		"fulfillment_service": "manual",
		"option1": "Mint green",
		"taxable": true,
		"grams": 4917,
		"image_id": null,
		"weight": 4.917,
		"weight_unit": "kg",
		"inventory_item_id": 6015629699,
		"inventory_quantity": 134,
		"old_inventory_quantity": 134,
		"requires_shipping": true,
	}*/
	const {inventory_quantity} = variationObj;
	const amountSold = Math.round(random(inventory_quantity / 4, inventory_quantity / 3));

	const decimalAmount = amountSold / inventory_quantity;
	const percent = Math.round(decimalAmount * 100);
	return (
			<Card key={`variation-view-${index}`} styles={{width: 150}}>
				<Text style={material.body1}>{`    Variation ${variationObj.position}: ${variationObj.title}`}</Text>
				<Text style={material.caption}>{`        Price: $${variationObj.price}`}</Text>
				<Text style={material.caption}>{`        Weight: ${variationObj.weight} ${variationObj.weight_unit}`}</Text>
				{
					variationObj.require_shipping === true
							? <Text style={material.caption}>{`        This product require shipping.`}</Text>
							: <Text style={material.caption}>{`        This product require does not shipping.`}</Text>
				}
				<View style={{paddingTop: 5, paddingLeft: 25, paddingBottom: 10}}>
					<ProgressBar progress={decimalAmount} width={75}/>
					<Text style={material.caption}>{percent}% Sold ({amountSold}/{inventory_quantity})</Text>
				</View>

			</Card>
	)
}

function collectionAndBody(title, body, totalInventory, amountSold) {
	/* title: String
	*  body: String
	*  Return's a View with the proper styling*/

	let bodyContent = body;
	if (bodyContent === "") {
		bodyContent = "Hmm, no description here."
	}

	const decimalAmount = amountSold / totalInventory;
	const percent = Math.round(decimalAmount * 100);
	return (
			<View style={{flex: 1, flexDirection: 'column'}}>
				<View style={Styles.titleAndDescription}>
					<Text style={material.body1}>{title}</Text>
					<Text style={material.caption}>{bodyContent}</Text>
				</View>
				<View style={{paddingTop: 9}}>
					<ProgressBar progress={decimalAmount} width={100}/>
					<Text style={material.caption}>{percent}% Sold ({amountSold}/{totalInventory})</Text>
				</View>
			</View>
	)

}

const Styles = {
	backButton: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
		padding: 10
	},
	centerVertically: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center'
	},
	imageRight: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'stretch',
	},
	titleAndDescription: {
		flex: 1,
		flexDirection: 'column',
		paddingTop: 20,
	}
};

export default ProductCardUncollapsed;