import React from 'react';
import {TouchableOpacity} from 'react-native';
import {Container, Content, Text} from "native-base";
import {withNavigation} from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';
import {material} from 'react-native-typography'
import {connect} from "react-redux";
import Loading from "../../Loading";
import {getProducts} from "../../../action/products";
import ProductListDashboard from "./ProductListDashboard";
import { addSingleProductId } from "../../../action/products";

/* This Component Takes the list of ids and loads the rest of the objects */
class ProductListSyncingComponent extends React.Component {
	static navigationOptions = ({navigation, screenProps}) => ({
		title: '',
		tintColor: 'transparent',
		headerLeft: (
				<React.Fragment>
					<TouchableOpacity onPress={() => navigation.goBack(null)}>
						<Icon style={Styles.backButton} size={30} name='angle-left'/>
					</TouchableOpacity>
					<TouchableOpacity onPress={() => navigation.goBack(null)}>
						<Text style={[material.caption, {color: 'white'}]}>Back</Text>
					</TouchableOpacity>
				</React.Fragment>
		),
		headerStyle: {
			backgroundColor: '#5E6BC7',
		},

	});
	render() {

		if (this.props.isLoading) {
			this.props.dispatch(getProducts(this.props.productIdArray));
		}
		const {isLoading} = this.props;
		if (isLoading){
			return (
					<Loading/>
			)
		}
		else {
			return (<ProductListDashboard/>);
		}
	}
}

const Styles = {
	backButton: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
		padding: 10,
		color: "white"
	},
};

function mapStateToProps({products}) {
	let productIdArray = extractProductIds(products.productIds);
	return {
		productIdArray: productIdArray,
		isLoading: products.isLoading,
		singleProductId: products.singleProductId,
	};
}

function extractProductIds(productIds) {
	/* this function takes a productsId object and extracts an array of the produce_ids*/

	let productIdArray = [];
	if (productIds !== undefined && productIds.collects !== undefined) {
		productIdArray = productIds.collects.map(obj => {
			return obj.product_id;
		});
	}
	return productIdArray;
}

export default connect(mapStateToProps)(withNavigation(ProductListSyncingComponent));