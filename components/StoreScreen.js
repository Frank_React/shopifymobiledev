import React from 'react';
import {Image} from 'react-native';
import SideBar from "./Sidebar"
import {Button, Container, Icon, View} from "native-base";
import CategoryDashboard from './store/category/CategoryDashboard';

const alertButton = (
		<View style={{marginTop: 2, marginRight: 20 }}>
			<Image
					style={{width: 30, height: 30}}
					source={{uri: 'https://i.imgur.com/6WOFxzO.png'}}/>
		</View>
);

		const shopifyLogo = (
				<Image
						style={{width: 150, height: 40}}
						source={{uri: 'https://i.imgur.com/w7IqtHU.png'}}/>
		);
// this is the left side of the navigation bar
const headerLeftMain = (
		<React.Fragment>
			{shopifyLogo}
		</React.Fragment>
);
export default class StoreScreen extends React.Component {
	static navigationOptions = ({navigation, screenProps}) => ({
		headerLeft: (<View style={{marginTop: 2}}>
			<Button
					transparent
					onPress={() => navigation.navigate("SideBar")}
			>
				<Icon name="menu" style={{color: "white"}}/>
			</Button>
		</View>),
		headerTitle: headerLeftMain,
		headerRight: alertButton,
		headerStyle: {
			backgroundColor: '#5E6BC7',
		},
	});

	render() {
		return (
				<Container>
					<CategoryDashboard/>
				</Container>
		);
	}

}
